﻿using Microsoft.AspNetCore.Hosting;
using System;
using System.IO;
using System.Threading.Tasks;

namespace AirsoftAmoApi
{
    public class Program
	{
		public static async Task Main(string[] args)
		{
			await new WebHostBuilder()
				.CaptureStartupErrors(true)
				.SuppressStatusMessages(false)
				.UseWebRoot(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot"))
                .UseContentRoot(Directory.GetCurrentDirectory())
				.UseEnvironment(EnvironmentName.Development)
				.UseKestrel()
				.UseShutdownTimeout(TimeSpan.FromSeconds(10d))
				.UseStartup<Startup>()
				.UseUrls("http://localhost:17652")
				.UseSockets()
				.Build().RunAsync();
		}
	}
}

namespace AirsoftAmoApi.Options
{
    public class TelegramNotifyOptions
    {
        public string BotToken = string.Empty;
        public string WebhookUrl = string.Empty;
    }
}
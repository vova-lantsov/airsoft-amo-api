﻿using AirsoftAmoApi.Extensions;
using AirsoftAmoApi.Models;
using AirsoftAmoApi.Models.Admin;
using AirsoftAmoApi.Models.MvcModels;
using AirsoftAmoApi.Models.ResponseModels;
using AirsoftAmoApi.Models.ResponseModels.Admin;
using AirsoftAmoApi.Services;
using Microsoft.AspNetCore.Mvc;
using MlkPwgen;
using MongoDB.Driver;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace AirsoftAmoApi.Controllers
{
    [Route("admin/api")]
    [SwaggerTag("Manage admins (with token)")]
    public class AdminApiController : Controller
	{
		private readonly AdminContext _adminContext;
        private readonly Context _context;

        public AdminApiController(AdminContext adminContext, Context context)
        {
            _adminContext = adminContext;
            _context = context;
        }
        
        [HttpPost("auth")]
        [SwaggerOperation("Authorizes admin",
            Consumes = new[] { "application/json" }, Schemes = new[] { "https" })]
        [SwaggerResponse(200, "Returns admin model with access token", typeof(AuthResponseModel))]
        [SwaggerResponse(400)]
        [SwaggerResponse(404, "Admin was not found in database")]
        [SwaggerResponse(403, "Password is incorrect")]
        public async Task<ActionResult<AuthResponseModel>> Auth(
            [FromBody, SwaggerParameter(Required = true)] AuthorizeModel model)
        {
            if (!ValidateWith<AuthorizeModel, AuthorizeModelValidator>(ref model))
                return BadRequest(ModelState);

            var admin = await _adminContext.Admins.Find(a => a.Login == model.Login).SingleOrDefaultAsync();
            if (admin == default)
            {
                return NotFound();
            }

            var adminCredentials = await _adminContext.AdminCredentials.Find(ac => ac.AdminId == admin.Id).SingleOrDefaultAsync();
            if (PasswordHashExtensions.HashPassword(model.Password, adminCredentials.Salt) != adminCredentials.PasswordHash)
            {
                return StatusCode(403);
            }

            string accessToken = PasswordGenerator.Generate(100);
            await _adminContext.AdminSessions.InsertOneAsync(new Session { AccessToken = accessToken, AdminId = admin.Id });

            return new AuthResponseModel
            {
                AccessToken = accessToken,
                Avatar = admin.Avatar,
                Id = admin.Id,
                CanceledOrders = admin.CanceledOrders,
                CompletedOrders = admin.CompletedOrders,
                FirstName = admin.FirstName,
                LastName = admin.LastName,
                Type = admin.Type,
                WordsSince = admin.WorksSince,
                AdminMonthsActivity = admin.AdminMonthsActivity
            };
        }
        
        [HttpGet("getAllOrders")]
        [SwaggerOperation("Returns all orders created in database",
            Consumes = new[] { "application/json" }, Schemes = new[] { "https" })]
        public async Task<ActionResult<GetAllOrdersResponseModel>> GetAllOrders(
            [FromQuery, SwaggerParameter("Length: 100", Required = true)] string accessToken)
        {
            if (accessToken == null || accessToken.Length != 100)
                return BadRequest();

            if (!await _adminContext.AdminSessions.Find(ass => ass.AccessToken == accessToken).AnyAsync())
            {
                return StatusCode(403);
            }

            return new GetAllOrdersResponseModel
            {
                Orders = await _context.Orders.Find(FilterDefinition<Order>.Empty).ToListAsync(),
                Stats = await _context.Stats.Find(FilterDefinition<Stat>.Empty).ToListAsync()
            };
        }
        
        [HttpPost("product")]
        [SwaggerOperation("Adds product to the database",
            Consumes = new[] { "application/json" }, Schemes = new[] { "https" })]
        public async Task<ActionResult<Product>> AddProduct([FromBody] AddProductModel model)
        {
            if (!ValidateWith<AddProductModel, AddProductModelValidator>(ref model))
                return BadRequest(ModelState);

            if (!await _adminContext.AdminSessions.Find(ass => ass.AccessToken == model.AccessToken).AnyAsync())
            {
                return StatusCode(403);
            }

            var productsCount = await _context.Products.CountDocumentsAsync(FilterDefinition<Product>.Empty);
            var product = new Product
            {
                AmountLeft = model.AmountLeft,
                Category = model.Category,
                Comments = new List<Comment>(),
                ImgUrl = model.ImgUrl,
                ProductDescription = model.ProductDescription,
                ProductId = productsCount + 1L,
                ProductName = model.ProductName,
                ProductPrice = model.ProductPrice,
                Rating = 0
            };
            await _context.Products.InsertOneAsync(product);
            return product;
        }

        [HttpPut("product")]
        public async Task<ActionResult<Product>> UpdateProduct([FromBody] UpdateProductModel model)
        {
            if (!ValidateWith<UpdateProductModel, UpdateProductModelValidator>(ref model))
                return BadRequest(ModelState);

            if (!await _adminContext.AdminSessions.Find(ass => ass.AccessToken == model.AccessToken).AnyAsync())
            {
                return StatusCode(403);
            }

            var r = await _context.Products.UpdateOneAsync(p => p.ProductId == model.ProductId,
                Builders<Product>.Update.Set(p => p.ProductDescription, model.ProductDescription)
                .Set(p => p.ProductName, model.ProductName).Set(p => p.Category, model.Category)
                .Set(p => p.ProductPrice, model.ProductPrice).Set(p => p.AmountLeft, model.AmountLeft)
                .Set(p => p.ImgUrl, model.ImgUrl));
            return await _context.Products.Find(p => p.ProductId == model.ProductId).SingleOrDefaultAsync();
        }
        
        [HttpGet("getAllAdmins")]
        public async Task<ActionResult<List<Admin>>> GetAllAdmins([FromQuery] string accessToken)
        {
            if (accessToken == null || accessToken.Length != 100)
                return BadRequest();

            if (!await _adminContext.AdminSessions.Find(ass => ass.AccessToken == accessToken).AnyAsync())
            {
                return StatusCode(403);
            }

            return await _adminContext.Admins.Find(FilterDefinition<Admin>.Empty).ToListAsync();
        }

        [HttpPut("adminType")]
        public async Task<ActionResult> ChangeAdminType([FromBody] ChangeAdminTypeModel model)
        {
            if (!ValidateWith<ChangeAdminTypeModel, ChangeAdminTypeModelValidator>(ref model))
                return BadRequest(ModelState);

            if (!await _adminContext.Admins.Find(a => a.Id == model.AdminId).AnyAsync())
            {
                return NotFound();
            }

            await _adminContext.Admins.UpdateOneAsync(a => a.Id == model.AdminId,
                Builders<Admin>.Update.Set(a => a.Type, model.NewType));
            return Ok();
        }
        
        [HttpPost("logout")]
        public async Task<ActionResult> Logout([FromBody] LogoutModel model)
        {
            if (!ValidateWith<LogoutModel, LogoutModelValidator>(ref model))
                return BadRequest(ModelState);

            await _adminContext.AdminSessions.DeleteOneAsync(ass => ass.AccessToken == model.AccessToken);
            return Ok();
        }

        [HttpPut("order/complete")]
        public async Task<ActionResult<CompleteOrderResponseModel>> CompleteOrder([FromBody] CompleteOrderModel model)
        {
            if (!ValidateWith<CompleteOrderModel, CompleteOrderModelValidator>(ref model))
                return BadRequest(ModelState);

            if (!await _adminContext.AdminSessions.Find(ass => ass.AccessToken == model.AccessToken).AnyAsync())
            {
                return StatusCode(403);
            }

            var order = await _context.Orders.Find(o => o.Id == model.OrderId).SingleOrDefaultAsync();
            if (order == default)
            {
                return NotFound();
            }

            if (order.Completed)
            {
                return Ok();
            }
            order.Completed = true;

            await _context.Orders.UpdateOneAsync(o => o.Id == model.OrderId,
                Builders<Order>.Update.Set(o => o.Completed, true));
            await _adminContext.Admins.UpdateOneAsync(a => a.Id == model.AdminId,
                Builders<Admin>.Update.Inc(a => a.CompletedOrders, 1L));
            var admin = await _adminContext.Admins.Find(a => a.Id == model.AdminId).SingleOrDefaultAsync();

            return new CompleteOrderResponseModel
            {
                Order = order,
                Admin = admin
            };
        }

        [HttpPut("order/cancel")]
        public async Task<ActionResult<CompleteOrderResponseModel>> CancelOrder([FromBody] CompleteOrderModel model)
        {
            if (!ValidateWith<CompleteOrderModel, CompleteOrderModelValidator>(ref model))
                return BadRequest(ModelState);

            if (!await _adminContext.AdminSessions.Find(ass => ass.AccessToken == model.AccessToken).AnyAsync())
            {
                return StatusCode(403);
            }

            var order = await _context.Orders.Find(o => o.Id == model.OrderId).SingleOrDefaultAsync();
            if (order == default)
            {
                return NotFound();
            }

            if (!order.Completed)
            {
                return Ok();
            }
            order.Completed = false;

            await _context.Orders.UpdateOneAsync(o => o.Id == model.OrderId,
                Builders<Order>.Update.Set(o => o.Completed, false));
            await _adminContext.Admins.UpdateOneAsync(a => a.Id == model.AdminId,
                Builders<Admin>.Update.Inc(a => a.CanceledOrders, 1L));
            var admin = await _adminContext.Admins.Find(a => a.Id == model.AdminId).SingleOrDefaultAsync();

            return new CompleteOrderResponseModel
            {
                Order = order,
                Admin = admin
            };
        }

        [HttpPut("name")]
        public async Task<ActionResult<Admin>> ChangeAdminName([FromBody] ChangeAdminNameModel model)
        {
            if (!ValidateWith<ChangeAdminNameModel, ChangeAdminNameModelValidator>(ref model))
                return BadRequest(ModelState);

            if (!await _adminContext.AdminSessions.Find(ass => ass.AccessToken == model.AccessToken).AnyAsync())
            {
                return StatusCode(403);
            }

            await _adminContext.Admins.UpdateOneAsync(a => a.Id == model.AdminId,
                Builders<Admin>.Update.Set(a => a.LastName, model.LastName)
                .Set(a => a.FirstName, model.FirstName));
            return await _adminContext.Admins.Find(a => a.Id == model.AdminId).SingleOrDefaultAsync();
        }

        [HttpPut("avatar")]
        public async Task<ActionResult<Admin>> ChangeAdminAvatar([FromBody] ChangeAdminAvatarModel model)
        {
            if (!ValidateWith<ChangeAdminAvatarModel, ChangeAdminAvatarModelValidator>(ref model))
                return BadRequest(ModelState);

            if (!await _adminContext.AdminSessions.Find(ass => ass.AccessToken == model.AccessToken).AnyAsync())
            {
                return StatusCode(403);
            }

            await _adminContext.Admins.UpdateOneAsync(a => a.Id == model.AdminId,
                Builders<Admin>.Update.Set(a => a.Avatar, model.Avatar));
            return await _adminContext.Admins.Find(a => a.Id == model.AdminId).SingleOrDefaultAsync();
        }

        [HttpPut("password")]
        public async Task<ActionResult> ChangeAdminPassword([FromBody] ChangeAdminPasswordModel model)
        {
            if (!ValidateWith<ChangeAdminPasswordModel, ChangeAdminPasswordModelValidator>(ref model))
                return BadRequest(ModelState);

            if (!await _adminContext.AdminSessions.Find(ass => ass.AccessToken == model.AccessToken).AnyAsync())
            {
                return StatusCode(403);
            }

            var adminCredentials = await _adminContext.AdminCredentials.Find(ac => ac.AdminId == model.AdminId).SingleOrDefaultAsync();

            var passwordWithSalt = string.Concat(model.OldPassword, adminCredentials.Salt);
            var passwordWithSaltBytes = Encoding.UTF8.GetBytes(passwordWithSalt);
            byte[] hashBytes;
            using (var hash = new SHA256Managed())
            {
                hashBytes = hash.ComputeHash(passwordWithSaltBytes, 0, passwordWithSaltBytes.Length);
            }
            string hashedPasswordWithSalt = Encoding.UTF8.GetString(hashBytes);
            if (hashedPasswordWithSalt != adminCredentials.PasswordHash)
            {
                return StatusCode(403);
            }

            var newSalt = PasswordGenerator.Generate(8);
            passwordWithSalt = string.Concat(model.NewPassword, newSalt);
            passwordWithSaltBytes = Encoding.UTF8.GetBytes(passwordWithSalt);
            using (var hash = new SHA256Managed())
            {
                hashBytes = hash.ComputeHash(passwordWithSaltBytes, 0, passwordWithSaltBytes.Length);
            }

            hashedPasswordWithSalt = Encoding.UTF8.GetString(hashBytes);
            await _adminContext.AdminCredentials.UpdateOneAsync(ac => ac.AdminId == model.AdminId,
                Builders<Credential>.Update.Set(c => c.PasswordHash, hashedPasswordWithSalt)
                .Set(c => c.Salt, newSalt));
            return Ok();
        }

        [HttpPost("createAdmin")]
        [SwaggerOperation("Creates admin in database",
            Consumes = new[] { "application/json" }, Schemes = new[] { "https" })]
        [SwaggerResponse(200, "Returns new admin object", typeof(Admin))]
        [SwaggerResponse(400)]
        [SwaggerResponse(401, "Invalid access token")]
        [SwaggerResponse(403, "Moderator can't create admins")]
        public async Task<ActionResult<Admin>> CreateAdmin(
            [FromBody, SwaggerParameter(Required = true)] AddAdminModel model)
        {
            if (!ValidateWith<AddAdminModel, AddAdminModelValidator>(ref model))
                return BadRequest(ModelState);

            var session = await _adminContext.AdminSessions.Find(ass => ass.AccessToken == model.AccessToken).SingleOrDefaultAsync();
            if (session == default)
                return StatusCode(401);

            var lastAdminId = await _adminContext.Admins.Find(FilterDefinition<Admin>.Empty).Project(a => a.Id).SortByDescending(a => a.Id).Limit(1).FirstAsync();
            var currentAdminType = await _adminContext.Admins.Find(a => a.Id == session.AdminId).Project(a => a.Type).SingleAsync();

            if (currentAdminType != "Administrator")
                return StatusCode(403);

            var admin = new Admin
            {
                AdminMonthsActivity = Defaults.MonthlyStatistics,
                Avatar = model.Avatar,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Login = model.Login,
                Type = model.Type,
                WorksSince = DateTime.UtcNow.ToString("dd/MM/yyyy"),
                Id = lastAdminId + 1L
            };
            admin.CanceledOrders = admin.AdminMonthsActivity.Sum(ama => ama.Canceled);
            admin.CompletedOrders = admin.AdminMonthsActivity.Sum(ama => ama.Completed);
            await _adminContext.Admins.InsertOneAsync(admin);

            var credential = new Credential
            {
                AdminId = admin.Id,
                Salt = PasswordGenerator.Generate(8)
            };
            credential.PasswordHash = PasswordHashExtensions.HashPassword(model.Password, credential.Salt);
            await _adminContext.AdminCredentials.InsertOneAsync(credential);

            return admin;
        }

        [HttpGet("getStocks")]
        [SwaggerOperation("Returns data with statistics for last 30 days",
            Consumes = new[] { "application/json" }, Schemes = new[] { "https" })]
        [SwaggerResponse(200, type: typeof(GetStocksResponseModel))]
        [SwaggerResponse(400, "Access token is incorrect")]
        [SwaggerResponse(403, "Access token is invalid")]
        public async Task<ActionResult<GetStocksResponseModel>> GetStocks(
            [FromQuery, SwaggerParameter("Length: 100", Required = true)] string accessToken)
        {
            if (accessToken == null || accessToken.Length != 100)
                return BadRequest();

            if (!await _adminContext.AdminSessions.Find(ass => ass.AccessToken == accessToken).AnyAsync())
                return StatusCode(403);

            var dailyStatistics = await _context.DailyStatistics.Find(FilterDefinition<DailyStatistic>.Empty).ToListAsync();
            var convertedSpends = new List<int>();
            var convertedNewClients = new List<int>();
            var convertedSales = new List<int>();
            var dates = new List<string>();
            foreach (var dailyStatistic in dailyStatistics)
            {
                convertedSpends.Add(dailyStatistic.TotalSpent);
                convertedNewClients.Add(dailyStatistic.TotalOrders);
                convertedSales.Add(dailyStatistic.TotalPrice);
                dates.Add(dailyStatistic.Date);
            }

            return new GetStocksResponseModel
            {
                Series = dates,
                Spends = new GetStocksResponseModel.GetStocksResponseModelItem
                {
                    Data = convertedSpends,
                    TotalValue = convertedSpends.Sum().ToString()
                },
                NewClients = new GetStocksResponseModel.GetStocksResponseModelItem
                {
                    Data = convertedNewClients,
                    TotalValue = convertedNewClients.Sum().ToString()
                },
                Sales = new GetStocksResponseModel.GetStocksResponseModelItem
                {
                    Data = convertedSales,
                    TotalValue = convertedSales.Sum().ToString()
                }
            };
        }

        [HttpDelete("moderator")]
        [SwaggerOperation("Deletes moderator from database", "This method won't delete admins",
            Consumes = new[] { "application/json" }, Schemes = new[] { "https" })]
        [SwaggerResponse(200, "Moderator was successfully deleted")]
        [SwaggerResponse(400)]
        [SwaggerResponse(401, "Invalid access token")]
        [SwaggerResponse(403, "Moderator tried to delete another moderator or admin, or admin tried to delete another admin, or anyone tried to delete itself")]
        [SwaggerResponse(404, "Admin was not found")]
        public async Task<ActionResult> DeleteModerator(
            [FromBody, SwaggerParameter(Required = true)] DeleteModeratorModel model)
        {
            if (!ValidateWith<DeleteModeratorModel, DeleteModeratorModelValidator>(ref model))
                return BadRequest(ModelState);

            var session = await _adminContext.AdminSessions.Find(ass => ass.AccessToken == model.AccessToken).SingleOrDefaultAsync();
            if (session == default)
                return StatusCode(401);

            var currentAdmin = await _adminContext.Admins.Find(a => a.Id == session.AdminId).SingleOrDefaultAsync();

            Expression<Func<Admin, bool>> adminExpression = a => a.Id == model.ModeratorId;
            var admin = await _adminContext.Admins.Find(adminExpression).SingleOrDefaultAsync();
            if (admin == default)
                return NotFound();

            if (currentAdmin.Id == admin.Id || currentAdmin.Type == "Moderator" || admin.Type == "Administrator")
                return StatusCode(403);

            await _adminContext.Admins.DeleteOneAsync(adminExpression);
            return Ok();
        }

        [HttpDelete("product")]
        [SwaggerOperation("Deletes product from database", "If product does not exist, this method will also return 200 OK. No need for 404 Not Found code.",
            Consumes = new[] { "application/json" }, Schemes = new[] { "https" })]
        [SwaggerResponse(200, "Product was deleted")]
        [SwaggerResponse(400)]
        [SwaggerResponse(401, "Invalid access token")]
        public async Task<ActionResult> DeleteProduct(
            [FromBody, SwaggerParameter(Required = true)] DeleteProductModel model)
        {
            if (!ValidateWith<DeleteProductModel, DeleteProductModelValidator>(ref model))
                return BadRequest(ModelState);

            if (!await _adminContext.AdminSessions.Find(ass => ass.AccessToken == model.AccessToken).AnyAsync())
                return StatusCode(401);

            await _context.Products.DeleteOneAsync(p => p.ProductId == model.ProductId);
            return Ok();
        }

        private bool ValidateWith<M, V>(ref M model) where M : class, new() where V : FluentValidation.AbstractValidator<M>, new()
        {
            if (model == null)
            {
                ModelState.TryAddModelError("model", "null");
                return false;
            }

            var validator = new V();
            var validationResult = validator.Validate(model);
            if (!validationResult.IsValid)
            {
                foreach (var error in validationResult.Errors)
                    ModelState.TryAddModelError(error.PropertyName, error.ErrorMessage);
            }
            return validationResult.IsValid;
        }
    }
}

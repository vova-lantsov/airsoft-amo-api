using AirsoftAmoApi.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace AirsoftAmoApi.Controllers
{
    [Route("webhook")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class WebhookController : Controller
    {
        private readonly TelegramNotifyService _telegramNotifyService;

        public WebhookController(TelegramNotifyService telegramNotifyService)
        {
            _telegramNotifyService = telegramNotifyService;
        }

        [HttpPost]
        public async Task<ActionResult> TelegramWebhook([FromBody] Update update)
        {
            if (update == null)
                return NotFound();
            
            switch (update.Type)
            {
                case UpdateType.Message:
                    await _telegramNotifyService.MessageReceived(update.Message);
                    break;

                case UpdateType.CallbackQuery:
                    await _telegramNotifyService.CallbackQueryReceived(update.CallbackQuery);
                    break;
            }
            return Ok();
        }
    }
}
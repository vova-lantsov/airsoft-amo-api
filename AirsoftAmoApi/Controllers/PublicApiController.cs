﻿using AirsoftAmo.AirsoftAmoApi.Models.MvcModels;
using AirsoftAmoApi.Extensions;
using AirsoftAmoApi.Models;
using AirsoftAmoApi.Models.ResponseModels;
using AirsoftAmoApi.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;

namespace AirsoftAmoApi.Controllers
{
    [Route("api")]
    [ApiController]
	public class PublicApiController : Controller
	{
		private readonly Context _context;
		private readonly TelegramNotifyService _telegramNotifyService;
		private readonly Stopwatch _stopwatch;
		private readonly List<string> _logs = new List<string>();

		public PublicApiController(Context context, TelegramNotifyService telegramNotifyService, Stopwatch stopwatch)
		{
			_context = context;
			_telegramNotifyService = telegramNotifyService;
			_stopwatch = stopwatch;
		}

		[HttpGet("code")]
        [ApiExplorerSettings(IgnoreApi = true)]
		public ActionResult<object> GetCode()
		{
			return new { code = _telegramNotifyService.Code };
		}

		[HttpGet("products")]
        [ProducesResponseType(typeof(List<Product>), 200)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<List<Product>>> GetProducts()
		{
			_stopwatch.Start();
			var products = await _context.Products.Find(FilterDefinition<Product>.Empty).ToListAsync();
			_logs.Add($"Get all products from database: {_stopwatch.ElapsedMilliseconds} ms");
			await Logging();
			return products;
		}

		[HttpGet("product")]
        [ProducesResponseType(typeof(Product), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<Product>> GetProduct(long productId)
		{
			_stopwatch.Start();
			var product = await _context.Products.Find(p => p.ProductId == productId).SingleOrDefaultAsync();
			_logs.Add($"Get single product from database: {_stopwatch.ElapsedMilliseconds} ms");
			await Logging();
			if (product == default)
				return NotFound();
			
			return product;
		}

		[HttpPost("comment")]
        [ProducesResponseType(typeof(LeaveCommentResponseModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<LeaveCommentResponseModel>> LeaveComment(LeaveCommentModel model)
		{
			_stopwatch.Start();
			var leaveCommentModelValidator = new LeaveCommentModelValidator();
			var validationResult = leaveCommentModelValidator.Validate(model);
			_logs.Add($"Validate new comment model: {_stopwatch.ElapsedMilliseconds} ms");

			if (!validationResult.IsValid)
			{
				foreach (var error in validationResult.Errors)
				{
					ModelState.TryAddModelError(error.PropertyName, error.ErrorMessage);
					_logs.Add($"_Err_ {error.PropertyName}: `{error.ErrorMessage}`");
				}
				await Logging();
				return BadRequest(ModelState);
			}

			_stopwatch.Restart();
			Expression<Func<Product, bool>> productExpr = p => p.ProductId == model.ProductId;
			var product = await _context.Products.Find(productExpr).SingleOrDefaultAsync();
			_logs.Add($"Get single product: {_stopwatch.ElapsedMilliseconds} ms");

			if (product == default)
				return NotFound();

			_stopwatch.Restart();
			var comment = new Comment
			{
				CommentId = product.Comments.Count + 1,
				FirstName = model.GuestName,
				CommentText = model.CommentText,
				CommentDate = DateTime.UtcNow.ToString("dd/MM/yyyy")
			};
			await _context.Products.UpdateOneAsync(productExpr, Builders<Product>.Update.Push(p => p.Comments, comment));
			_stopwatch.Stop();
			_logs.Add($"Update single product (adds comment): {_stopwatch.ElapsedMilliseconds} ms");

			await Logging();

			await _telegramNotifyService.SendMessage(comment.FormatComment(product),
				(InlineKeyboardMarkup) new[]
				{
					new InlineKeyboardButton { Text = "Accept", CallbackData = $"comment_accept_{product.ProductId}_{comment.CommentId}" },
					new InlineKeyboardButton { Text = "Decline", CallbackData = $"comment_decline_{product.ProductId}_{comment.CommentId}" }
				});
			return new LeaveCommentResponseModel
			{
				CommentId = comment.CommentId,
				GuestName = model.GuestName,
				CommentText = model.CommentText,
				CommentDate = comment.CommentDate
			};
		}

		[HttpPost("order")]
        [ProducesResponseType(typeof(SubmitOrderResponseModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<SubmitOrderResponseModel>> SubmitOrder(Order order)
		{
			_stopwatch.Start();
            if (order == null)
                return BadRequest();

			var validator = new PublicOrderValidator();
			var result = validator.Validate(order);
			_logs.Add($"Validate new order model: {_stopwatch.ElapsedMilliseconds} ms");

			if (!result.IsValid)
			{
				foreach (var error in result.Errors)
				{
					ModelState.TryAddModelError(error.PropertyName, error.ErrorMessage);
					_logs.Add($"_Err_ {error.PropertyName}: `{error.ErrorMessage}`");
				}
				await Logging();
				return BadRequest(ModelState);
			}
			
			_stopwatch.Restart();
			var count = await _context.Orders.CountDocumentsAsync(FilterDefinition<Order>.Empty);
			_logs.Add($"Count documents: {_stopwatch.ElapsedMilliseconds} ms");

			order.Id = count + 1L;
			_stopwatch.Restart();
			await _context.Orders.InsertOneAsync(order);
			_logs.Add($"Insert single order: {_stopwatch.ElapsedMilliseconds} ms");

			_stopwatch.Restart();
			var products = await _context.Products.Find(Builders<Product>.Filter.In(p => p.ProductId, order.Products.Select(it => it.ProductId))).ToListAsync();
			_logs.Add($"Get all products from order: {_stopwatch.ElapsedMilliseconds} ms");

			await Logging();

			await _telegramNotifyService.SendMessage(order.FormatOrder(products, out var orderId),
				(InlineKeyboardMarkup) new[]
				{
					new InlineKeyboardButton { Text = "Accept", CallbackData = $"order_accept_{orderId}" },
					new InlineKeyboardButton { Text = "Decline", CallbackData = $"order_decline_{orderId}" }
				});
			return new SubmitOrderResponseModel { OrderId = orderId };
		}

		private async Task Logging()
		{
			await _telegramNotifyService.SendLog($"*Logging*\n\n{string.Join('\n', _logs)}");
		}
	}
}

﻿using AirsoftAmoApi.Models;
using Microsoft.Extensions.Hosting;
using MongoDB.Driver;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace AirsoftAmoApi.Services.Hosted
{
    public class DailyTimer : IHostedService
    {
        private readonly Timer _timer;
        private readonly Random _random = new Random(DateTime.UtcNow.Millisecond);

        public DailyTimer(IHostingEnvironment env, Context context)
        {
            _timer = new Timer(_ =>
            {
                var date = DateTime.UtcNow;
                var statistic = new DailyStatistic
                {
                    Date = date.ToString("dd/MM/yyyy")
                };
                if (!env.IsProduction())
                {
                    statistic.TotalOrders = _random.Next(101);
                    statistic.TotalPrice = _random.Next(101) * 500;
                    statistic.TotalSpent = _random.Next(101) * 100;
                }
                context.DailyStatistics.InsertOne(statistic);
                context.DailyStatistics.DeleteOne(Builders<DailyStatistic>.Filter.Eq(ds => ds.Date, date.AddDays(-30d).ToString("dd/MM/yyyy")));
            }, null, new TimeSpan(24, 0, 5) - DateTime.UtcNow.TimeOfDay, TimeSpan.FromDays(1d));
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer.Change(-1, -1);
            _timer.Dispose();
            return Task.CompletedTask;
        }
    }
}

using AirsoftAmoApi.Extensions;
using AirsoftAmoApi.Models;
using AirsoftAmoApi.Options;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace AirsoftAmoApi.Services
{
    public class TelegramNotifyService
    {
        private readonly TelegramBotClient _client;
        private readonly Context _context;
        private readonly Random _random = new Random(DateTime.UtcNow.Millisecond);
        private readonly string _webhookUrl;
        private long _chatId = default;
		private bool _logging = false;

        public string Code { get; private set; } = string.Empty;

        public async Task MessageReceived(Message value)
        {
            var split = value.Text.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            if (split.Length == 3 && split[0] == "sudo" && split[1] == "request" && split[2] == "key")
            {
                Code = _random.Next(100, 1000).ToString();
            }
            else if (split.Length == 6 && split[0] == "sudo" && split[1] == "kiss" && split[2] == "this" && split[3] == "girl" && split[4] == "--with-key" && split[5] == Code)
            {
                _chatId = value.Chat.Id;
                Code = string.Empty;
                await _client.SendTextMessageAsync(_chatId, "*This girl gave you the access to herself*", ParseMode.Markdown);
            }
            else if (split.Length == 2 && split[1] == "logging")
            {
                switch (split[0])
                {
                    case "enable":
                        _logging = true;
                        break;

                    case "disable":
                        _logging = false;
                        break;
                }
            }
        }

        public async Task CallbackQueryReceived(CallbackQuery callbackQuery)
        {
            var chatId = callbackQuery.Message.Chat.Id;
            var messageId = callbackQuery.Message.MessageId;
            var split = callbackQuery.Data.Split('_');
            
            switch (split[0])
            {
                case "order" when split.Length == 3 && long.TryParse(split[2], out var orderId):
                {
                    var order = await _context.Orders.Find(o => o.Id == orderId).SingleOrDefaultAsync();
                    if (order == default)
                    {
                        await _client.EditMessageTextAsync(chatId, messageId, "*Order was deleted*", ParseMode.Markdown);
                        return;
                    }

                    var products = await _context.Products.Find(Builders<Product>.Filter.In(p => p.ProductId, order.Products.Select(it => it.ProductId))).ToListAsync();
                    var newText = order.FormatOrder(products, out _);
                    var status = split[1];
                    await _client.EditMessageTextAsync(chatId, messageId, $"{newText}\n\n*{(split[1] == "accept" ? "ACCEPT" : "DECLIN")}ED*", ParseMode.Markdown);
                    if (split[1] == "decline")
                    {
                        await _context.Orders.DeleteOneAsync(Builders<Order>.Filter.Eq(o => o.Id, orderId));
                    }
                    break;
                }
                case "comment" when split.Length == 4 && long.TryParse(split[2], out var productId)
                    && long.TryParse(split[3], out var commentId):
                {
                    var product = await _context.Products.Find(p => p.ProductId == productId).SingleOrDefaultAsync();
                    if (product == default)
                    {
                        await _client.EditMessageTextAsync(chatId, messageId, "*This comment's product was not found*", ParseMode.Markdown);
                        return;
                    }

                    var comment = product.Comments.Find(c => c.CommentId == commentId);
                    if (comment == default)
                    {
                        await _client.EditMessageTextAsync(chatId, messageId, "*This comment was deleted*", ParseMode.Markdown);
                        return;
                    }

                    var newText = comment.FormatComment(product);
                    await _client.EditMessageTextAsync(chatId, messageId, $"{newText}\n\n*{(split[1] == "accept" ? "ACCEPT" : "DECLIN")}ED*", ParseMode.Markdown);
                    
                    if (split[1] == "decline")
                    {
                        Expression<Func<Product, bool>> filterExpr = p => p.ProductId == productId;
                        await _context.Products.UpdateOneAsync(filterExpr,
                            Builders<Product>.Update.Pull("Comments", new BsonDocument("CommentId", commentId)));
                        await _context.Products.UpdateOneAsync(filterExpr,
                            Builders<Product>.Update.Inc("Comments.$[c].CommentId", -1L),
                            new UpdateOptions
                            {
                                ArrayFilters = new[]
                                {
                                    new BsonDocumentArrayFilterDefinition<Comment>(new BsonDocument
                                    {
                                        {
                                            "c.CommentId", new BsonDocument("$gt", commentId)
                                        }
                                    })
                                }
                            });
                    }
                    break;
                }
            }
        }

        public TelegramNotifyService(IOptions<TelegramNotifyOptions> options, Context context)
        {
            _client = new TelegramBotClient(options.Value.BotToken);
            _webhookUrl = options.Value.WebhookUrl;
            _context = context;
        }

        public async Task SendMessage(string message, IReplyMarkup? markup = null)
        {
            if (_chatId != default)
            {
                await _client.SendTextMessageAsync(_chatId, message, ParseMode.Markdown, replyMarkup: markup);
            }
        }

        public async Task SendLog(string logMessage)
        {
            if (_logging && _chatId != default)
            {
                await _client.SendTextMessageAsync(_chatId, logMessage, ParseMode.Markdown);
            }
        }

        public async Task Start()
        {
            await _client.SetWebhookAsync(_webhookUrl, allowedUpdates: new[] { UpdateType.Message, UpdateType.CallbackQuery });
        }

        public async Task Stop()
        {
            await _client.DeleteWebhookAsync();
        }
    }
}
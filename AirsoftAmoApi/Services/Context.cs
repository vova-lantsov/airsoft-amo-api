﻿using AirsoftAmoApi.Models;
using Microsoft.AspNetCore.Hosting;
using MongoDB.Driver;

namespace AirsoftAmoApi.Services
{
	public class Context
	{
		public readonly IMongoCollection<Product> Products;
		public readonly IMongoCollection<Stat> Stats;
		public readonly IMongoCollection<Order> Orders;
        public readonly IMongoCollection<DailyStatistic> DailyStatistics;

		public Context(IHostingEnvironment env)
		{
			var mongo = new MongoClient(env.IsProduction()
				? "mongodb://adMIN:vvrAsuDSciArK23ZGkpQ3r7AFiaM8vR77ySx@localhost:27017"
				: "mongodb://localhost:27017");
			var db = mongo.GetDatabase("AirsoftAmo");
			Products = db.GetCollection<Product>("Products");
			Stats = db.GetCollection<Stat>("Stats");
			Orders = db.GetCollection<Order>("Orders");
            DailyStatistics = db.GetCollection<DailyStatistic>("DailyStatistics");
		}
	}
}

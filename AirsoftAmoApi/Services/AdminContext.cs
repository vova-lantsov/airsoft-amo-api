﻿using AirsoftAmoApi.Models.Admin;
using Microsoft.AspNetCore.Hosting;
using MongoDB.Driver;

namespace AirsoftAmoApi.Services
{
	public class AdminContext
	{
		public readonly IMongoCollection<Admin> Admins;
        public readonly IMongoCollection<Credential> AdminCredentials;
        public readonly IMongoCollection<Session> AdminSessions;

		public AdminContext(IHostingEnvironment env)
		{
			var mongo = new MongoClient(env.IsDevelopment()
				? "mongodb://localhost:27017"
				: "mongodb://default:vvrAsuDSciArK23ZGkpQ3r7AFiaM8vR77ySx@localhost:27017");
			var db = mongo.GetDatabase("AirsoftAmoAdmin");
			Admins = db.GetCollection<Admin>("admins");
            AdminCredentials = db.GetCollection<Credential>("adminCredentials");
            AdminSessions = db.GetCollection<Session>("adminSessions");
		}
	}
}

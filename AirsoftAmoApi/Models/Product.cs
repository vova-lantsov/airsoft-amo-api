﻿using FluentValidation;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace AirsoftAmoApi.Models
{
	public class Product
	{
		[BsonId]
		public long ProductId;
		public string ProductName = string.Empty;
		public string Category = string.Empty;
		public string ProductPrice = string.Empty;
		public string ProductDescription = string.Empty;
		public string AmountLeft = string.Empty;
		public string ImgUrl = string.Empty;
		public int Rating;
		public List<Comment> Comments = new List<Comment>();
	}

	public class PublicProductValidator : AbstractValidator<Product>
	{
		public PublicProductValidator()
		{
			RuleFor(p => p.ProductId).Equal(0L);
			RuleFor(p => p.ProductPrice).Must(productPrice =>
			{
				return decimal.TryParse(productPrice, out _);
			});
			RuleFor(p => p.AmountLeft).Must(amountLeft =>
			{
				return int.TryParse(amountLeft, out _);
			});
			RuleFor(p => p.ImgUrl).Must(imgUrl =>
			{
				try
				{
					new Uri(imgUrl, UriKind.Absolute);
					return true;
				}
				catch (UriFormatException)
				{
					return false;
				}
			});
			RuleForEach(p => p.Comments).NotNull().Must(comment =>
			{
				return DateTime.TryParseExact(comment.CommentDate, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal, out _)
					&& comment.CommentId > 0L && comment.CommentText.Length >= 10;
			});
		}
	}
}

﻿using MongoDB.Bson.Serialization.Attributes;

namespace AirsoftAmoApi.Models
{
    public class DailyStatistic
    {
        [BsonId]
        public string Date = string.Empty;
        public int TotalPrice;
        public int TotalOrders;
        public int TotalSpent;
    }
}

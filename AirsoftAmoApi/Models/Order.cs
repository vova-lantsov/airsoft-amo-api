﻿using System.Collections.Generic;
using FluentValidation;
using MongoDB.Bson.Serialization.Attributes;
using PhoneNumbers;

namespace AirsoftAmoApi.Models
{
	public class Order
	{
		[BsonId]
		public long Id;
		public string Name = string.Empty;
		public string PhoneNumber = string.Empty;
		public string DeliveryLocation = string.Empty;
		public bool Completed;
		public List<OrderProduct> Products = new List<OrderProduct>();
	}

	public class PublicOrderValidator : AbstractValidator<Order>
	{
        public PublicOrderValidator()
        {
            RuleFor(o => o.Id).Equal(0L);
            RuleFor(o => o.Name).Length(2, 100);
            RuleFor(o => o.PhoneNumber).Must(phone =>
            {
                try
                {
                    var util = PhoneNumberUtil.GetInstance();
                    var parsed = util.Parse(phone, null);
                    return util.IsValidNumber(parsed);
                }
                catch
                {
                    return false;
                }
            });
            RuleFor(o => o.DeliveryLocation).NotEmpty();
            RuleFor(o => o.Completed).Equal(false);
            RuleForEach(o => o.Products).NotNull().Must(product =>
            {
                return product.ProductId > 0L && product.Amount > 0;
            });
        }
	}
}

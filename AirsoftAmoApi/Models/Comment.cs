﻿namespace AirsoftAmoApi.Models
{
	public class Comment
	{
		public long CommentId;
		public string FirstName = string.Empty;
		public string CommentText = string.Empty;
		public string CommentDate = string.Empty;
	}
}

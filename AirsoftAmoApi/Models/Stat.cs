﻿using MongoDB.Bson.Serialization.Attributes;

namespace AirsoftAmoApi.Models
{
    public class Stat
	{
        [BsonId]
        public string Name = string.Empty;
        public int ProductsSold;
	}
}

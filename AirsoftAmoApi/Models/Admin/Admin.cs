﻿using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace AirsoftAmoApi.Models.Admin
{
	public class Admin
	{
        [BsonId]
        public long Id;
        public string Login = string.Empty;
        public string FirstName = string.Empty;
        public string LastName = string.Empty;
        public string Avatar = string.Empty;
        public string Type = string.Empty;
        public long CompletedOrders;
        public long CanceledOrders;
        public string WorksSince = string.Empty;
        public List<MonthlyStatistic> AdminMonthsActivity = new List<MonthlyStatistic>();
	}
}

﻿using MongoDB.Bson.Serialization.Attributes;

namespace AirsoftAmoApi.Models.Admin
{
    public class Credential
    {
        [BsonId]
        public long AdminId;
        public string PasswordHash = string.Empty;
        public string Salt = string.Empty;
    }
}

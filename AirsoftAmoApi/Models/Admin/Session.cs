﻿using MongoDB.Bson.Serialization.Attributes;

namespace AirsoftAmoApi.Models.Admin
{
    public class Session
    {
        [BsonId]
        public string AccessToken = string.Empty;
        public long AdminId;
    }
}

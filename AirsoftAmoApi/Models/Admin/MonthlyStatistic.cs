﻿namespace AirsoftAmoApi.Models.Admin
{
    public class MonthlyStatistic
    {
        public string Name = string.Empty;
        public long Completed;
        public long Canceled;
    }
}

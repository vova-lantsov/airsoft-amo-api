﻿using FluentValidation;
using System;

namespace AirsoftAmoApi.Models.MvcModels
{
    public class AddProductModel
    {
        public string AccessToken = string.Empty;
        public string ProductName = string.Empty;
        public string Category = string.Empty;
        public string ProductPrice = string.Empty;
        public string ProductDescription = string.Empty;
        public string AmountLeft = string.Empty;
        public string ImgUrl = string.Empty;
    }

    public class AddProductModelValidator : AbstractValidator<AddProductModel>
    {
        public AddProductModelValidator()
        {
            RuleFor(m => m.AccessToken).Length(100);
            RuleFor(m => m.AmountLeft).Must(amountLeft => int.TryParse(amountLeft, out _));
            RuleFor(m => m.Category).NotEmpty();
            RuleFor(m => m.ImgUrl).Must(url =>
            {
                try
                {
                    new Uri(url, UriKind.Absolute);
                    return true;
                }
                catch
                {
                    return false;
                }
            });
            RuleFor(m => m.ProductName).Length(2, 100);
            RuleFor(m => m.ProductPrice).Must(price => decimal.TryParse(price, out _));
        }
    }
}

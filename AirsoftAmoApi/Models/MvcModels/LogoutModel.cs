﻿using FluentValidation;

namespace AirsoftAmoApi.Models.MvcModels
{
    public class LogoutModel
    {
        public string AccessToken = string.Empty;
    }

    public class LogoutModelValidator : AbstractValidator<LogoutModel>
    {
        public LogoutModelValidator()
        {
            RuleFor(m => m.AccessToken).Length(100);
        }
    }
}

using FluentValidation;

namespace AirsoftAmo.AirsoftAmoApi.Models.MvcModels
{
  public class LeaveCommentModel
  {
    public string GuestName = string.Empty;
    public long ProductId;
    public string CommentText = string.Empty;
  }

  public class LeaveCommentModelValidator : AbstractValidator<LeaveCommentModel>
  {
    public LeaveCommentModelValidator()
    {
      RuleFor(m => m.ProductId).GreaterThanOrEqualTo(1L);
      RuleFor(m => m.GuestName).Length(2, 100);
      RuleFor(m => m.CommentText).Length(4, 1000);
    }
  }
}
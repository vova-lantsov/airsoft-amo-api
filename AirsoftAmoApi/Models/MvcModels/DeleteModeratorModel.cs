﻿using FluentValidation;

namespace AirsoftAmoApi.Models.MvcModels
{
    public class DeleteModeratorModel
    {
        public string AccessToken = string.Empty;
        public long ModeratorId;
    }

    public class DeleteModeratorModelValidator : AbstractValidator<DeleteModeratorModel>
    {
        public DeleteModeratorModelValidator()
        {
            RuleFor(m => m.AccessToken).NotNull().Length(100);
            RuleFor(m => m.ModeratorId).GreaterThanOrEqualTo(1L);
        }
    }
}

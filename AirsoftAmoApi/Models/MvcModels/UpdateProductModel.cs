﻿using FluentValidation;
using System;

namespace AirsoftAmoApi.Models.MvcModels
{
    public class UpdateProductModel
    {
        public string AccessToken = string.Empty;
        public long ProductId;
        public string ProductName = string.Empty;
        public string Category = string.Empty;
        public string ProductPrice = string.Empty;
        public string ProductDescription = string.Empty;
        public string AmountLeft = string.Empty;
        public string ImgUrl = string.Empty;
    }

    public class UpdateProductModelValidator : AbstractValidator<UpdateProductModel>
    {
        public UpdateProductModelValidator()
        {
            RuleFor(m => m.AccessToken).Length(100);
            RuleFor(m => m.ProductId).GreaterThanOrEqualTo(1L);
            RuleFor(m => m.AmountLeft).Must(amountLeft => int.TryParse(amountLeft, out _));
            RuleFor(m => m.Category).NotEmpty();
            RuleFor(m => m.ImgUrl).Must(url =>
            {
                try
                {
                    new Uri(url, UriKind.Absolute);
                    return true;
                }
                catch
                {
                    return false;
                }
            });
            RuleFor(m => m.ProductName).Length(2, 100);
            RuleFor(m => m.ProductPrice).Must(price => decimal.TryParse(price, out _));
        }
    }
}

﻿using FluentValidation;

namespace AirsoftAmoApi.Models.MvcModels
{
	public class AuthorizeModel
	{
        public string Login = string.Empty;
        public string Password = string.Empty;
	}

    public class AuthorizeModelValidator : AbstractValidator<AuthorizeModel>
    {
        public AuthorizeModelValidator()
        {
            RuleFor(m => m.Login).Length(2, 60);
            RuleFor(m => m.Password).Length(8, 64);
        }
    }
}

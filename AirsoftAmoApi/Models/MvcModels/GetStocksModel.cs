﻿using FluentValidation;

namespace AirsoftAmoApi.Models.MvcModels
{
    public class GetStocksModel
    {
        public string AccessToken = string.Empty;
    }

    public class GetStocksModelValidator : AbstractValidator<GetStocksModel>
    {
        public GetStocksModelValidator()
        {
            RuleFor(m => m.AccessToken).NotNull().Length(100);
        }
    }
}

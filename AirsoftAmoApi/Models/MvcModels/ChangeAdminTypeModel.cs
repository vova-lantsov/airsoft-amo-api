﻿using FluentValidation;

namespace AirsoftAmoApi.Models.MvcModels
{
    public class ChangeAdminTypeModel
    {
        public string AccessToken = string.Empty;
        public long AdminId;
        public string NewType = string.Empty;
    }

    public class ChangeAdminTypeModelValidator : AbstractValidator<ChangeAdminTypeModel>
    {
        public ChangeAdminTypeModelValidator()
        {
            RuleFor(m => m.AccessToken).Length(100);
            RuleFor(m => m.AdminId).GreaterThanOrEqualTo(1L);
            RuleFor(m => m.NewType).Must(type => type == "Administrator" || type == "Moderator");
        }
    }
}

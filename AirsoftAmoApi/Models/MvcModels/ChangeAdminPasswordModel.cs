﻿using FluentValidation;

namespace AirsoftAmoApi.Models.MvcModels
{
    public class ChangeAdminPasswordModel
    {
        public string AccessToken = string.Empty;
        public long AdminId;
        public string OldPassword = string.Empty;
        public string NewPassword = string.Empty;
    }

    public class ChangeAdminPasswordModelValidator : AbstractValidator<ChangeAdminPasswordModel>
    {
        public ChangeAdminPasswordModelValidator()
        {
            RuleFor(m => m.AccessToken).Length(100);
            RuleFor(m => m.AdminId).GreaterThanOrEqualTo(1L);
            RuleFor(m => m.NewPassword).Length(8, 64);
            RuleFor(m => m.OldPassword).Length(8, 64);
        }
    }
}

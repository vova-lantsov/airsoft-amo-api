﻿using FluentValidation;

namespace AirsoftAmoApi.Models.MvcModels
{
    public class ChangeAdminNameModel
    {
        public string AccessToken = string.Empty;
        public long AdminId;
        public string FirstName = string.Empty;
        public string LastName = string.Empty;
    }

    public class ChangeAdminNameModelValidator : AbstractValidator<ChangeAdminNameModel>
    {
        public ChangeAdminNameModelValidator()
        {
            RuleFor(m => m.AccessToken).Length(100);
            RuleFor(m => m.AdminId).GreaterThanOrEqualTo(1L);
            RuleFor(m => m.FirstName).Length(2, 100);
            RuleFor(m => m.LastName).MaximumLength(100);
        }
    }
}

﻿using FluentValidation;
using System;

namespace AirsoftAmoApi.Models.MvcModels
{
    public class ChangeAdminAvatarModel
    {
        public string AccessToken = string.Empty;
        public long AdminId;
        public string Avatar = string.Empty;
    }

    public class ChangeAdminAvatarModelValidator : AbstractValidator<ChangeAdminAvatarModel>
    {
        public ChangeAdminAvatarModelValidator()
        {
            RuleFor(m => m.AccessToken).Length(100);
            RuleFor(m => m.AdminId).GreaterThanOrEqualTo(1L);
            RuleFor(m => m.Avatar).Must(avatar =>
            {
                try
                {
                    new Uri(avatar, UriKind.Absolute);
                    return true;
                }
                catch
                {
                    return false;
                }
            });
        }
    }
}

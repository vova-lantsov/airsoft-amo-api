﻿using FluentValidation;

namespace AirsoftAmoApi.Models.MvcModels
{
    public class DeleteProductModel
    {
        public string AccessToken = string.Empty;
        public long ProductId;
    }

    public class DeleteProductModelValidator : AbstractValidator<DeleteProductModel>
    {
        public DeleteProductModelValidator()
        {
            RuleFor(m => m.AccessToken).NotNull().Length(100);
            RuleFor(m => m.ProductId).GreaterThanOrEqualTo(1L);
        }
    }
}

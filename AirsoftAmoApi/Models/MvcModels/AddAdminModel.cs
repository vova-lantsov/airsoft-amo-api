﻿using AirsoftAmoApi.Extensions;
using FluentValidation;

namespace AirsoftAmoApi.Models.MvcModels
{
    public class AddAdminModel
    {
        public string AccessToken = string.Empty;
        public string FirstName = string.Empty;
        public string LastName = string.Empty;
        public string Login = string.Empty;
        public string Avatar = string.Empty;
        public string Type = string.Empty;
        public string Password = string.Empty;
    }

    public class AddAdminModelValidator : AbstractValidator<AddAdminModel>
    {
        public AddAdminModelValidator()
        {
            RuleFor(m => m.AccessToken).NotNull().Length(100);
            RuleFor(m => m.FirstName).NotNull().Length(2, 60);
            RuleFor(m => m.LastName).NotNull().Length(2, 60);
            RuleFor(m => m.Login).NotNull().Length(2, 30);
            RuleFor(m => m.Avatar).NotNull().Url();
            RuleFor(m => m.Type).NotNull().Must(type =>
            {
                return type == "Administrator" || type == "Moderator";
            }).WithMessage("Type must be 'Administrator' or 'Moderator'");
            RuleFor(m => m.Password).NotNull().Length(8, 64);
        }
    }
}

﻿using FluentValidation;

namespace AirsoftAmoApi.Models.MvcModels
{
    public class CompleteOrderModel
    {
        public string AccessToken = string.Empty;
        public long AdminId;
        public long OrderId;
    }

    public class CompleteOrderModelValidator : AbstractValidator<CompleteOrderModel>
    {
        public CompleteOrderModelValidator()
        {
            RuleFor(m => m.AccessToken).Length(100);
            RuleFor(m => m.AdminId).GreaterThanOrEqualTo(1L);
            RuleFor(m => m.OrderId).GreaterThanOrEqualTo(1L);
        }
    }
}

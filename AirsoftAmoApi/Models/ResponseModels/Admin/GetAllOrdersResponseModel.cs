﻿using System.Collections.Generic;

namespace AirsoftAmoApi.Models.ResponseModels.Admin
{
    public class GetAllOrdersResponseModel
    {
        public List<Order> Orders = new List<Order>();
        public List<Stat> Stats = new List<Stat>();
    }
}

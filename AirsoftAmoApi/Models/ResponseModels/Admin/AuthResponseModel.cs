﻿using AirsoftAmoApi.Models.Admin;
using System.Collections.Generic;

namespace AirsoftAmoApi.Models.ResponseModels.Admin
{
    public class AuthResponseModel
    {
        public long Id;
        public string FirstName = string.Empty;
        public string LastName = string.Empty;
        public string Avatar = string.Empty;
        public string Type = string.Empty;
        public long CompletedOrders;
        public long CanceledOrders;
        public string WordsSince = string.Empty;
        public string AccessToken = string.Empty;
        public List<MonthlyStatistic> AdminMonthsActivity = new List<MonthlyStatistic>();
    }
}

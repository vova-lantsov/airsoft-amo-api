﻿namespace AirsoftAmoApi.Models.ResponseModels
{
    public class LeaveCommentResponseModel
    {
        public long CommentId;
        public string GuestName = string.Empty;
        public string CommentText = string.Empty;
        public string CommentDate = string.Empty;
    }
}

﻿using System.Collections.Generic;

namespace AirsoftAmoApi.Models.ResponseModels
{
    public class GetStocksResponseModel
    {
        public List<string> Series = new List<string>();
        public GetStocksResponseModelItem Sales = new GetStocksResponseModelItem();
        public GetStocksResponseModelItem Spends = new GetStocksResponseModelItem();
        public GetStocksResponseModelItem NewClients = new GetStocksResponseModelItem();

        public class GetStocksResponseModelItem
        {
            public List<int> Data = new List<int>();
            public string TotalValue = string.Empty;
        }
    }
}

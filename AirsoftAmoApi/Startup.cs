﻿using AirsoftAmoApi.Extensions;
using AirsoftAmoApi.Models;
using AirsoftAmoApi.Models.Admin;
using AirsoftAmoApi.Options;
using AirsoftAmoApi.Services;
using AirsoftAmoApi.Services.Hosted;
using FluentValidation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Diagnostics;
using System.IO;

namespace AirsoftAmoApi
{
    public class Startup
	{
		public Startup()
		{
			ValidatorOptions.CascadeMode = CascadeMode.StopOnFirstFailure;
		}

		public void ConfigureServices(IServiceCollection services)
		{
			services.AddMvcCore().AddCors(options =>
			{
				options.AddPolicy(EnvironmentName.Development, policy =>
				{
					policy.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin();
				});
				options.AddPolicy(EnvironmentName.Production, policy =>
				{
					policy.AllowAnyHeader().WithMethods(HttpMethods.Get, HttpMethods.Post, HttpMethods.Put, HttpMethods.Delete).WithOrigins("https://airsoft.vova-lantsov.com");
				});
			}).AddJsonFormatters(settings =>
            {
                settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
            .AddApiExplorer();

			services.Configure<TelegramNotifyOptions>(options =>
			{
				options.BotToken = "750748559:AAFZgDoEtaiBhLlzMi2BGEI-_H_lUsG8wE8";
				options.WebhookUrl = "https://airsoft.vova-lantsov.com/webhook";
			});

			services.AddSingleton<Context>();
            services.AddSingleton<AdminContext>();
			services.AddSingleton<TelegramNotifyService>();
			services.AddTransient<Stopwatch>();
            
            services.AddSwaggerGen(options =>
            {
                options.DescribeAllParametersInCamelCase();
                options.SwaggerDoc("v1", new Info
                {
                    Title = "Airsoft Amo API",
                    Version = "v1",
                    Contact = new Contact
                    {
                        Email = "vova.lantsov.25@gmail.com",
                        Name = "Vova Lantsov",
                        Url = "https://t.me/vova_lantsov"
                    }
                });

                options.EnableAnnotations();
            });

            services.AddHostedService<DailyTimer>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApplicationLifetime lifetime, Context context, AdminContext adminContext, TelegramNotifyService telegramNotifyService)
        {
            if (!env.IsProduction())
            {
                if (context.Products.CountDocuments(FilterDefinition<Product>.Empty) == 0)
                    context.Products.InsertMany(Defaults.Products);

                if (adminContext.Admins.CountDocuments(FilterDefinition<Admin>.Empty) == 0)
                {
                    adminContext.Admins.InsertManyAsync(Defaults.Admins);
                    adminContext.AdminCredentials.InsertManyAsync(Defaults.AdminCredentials);
                }

                if (context.DailyStatistics.CountDocuments(FilterDefinition<DailyStatistic>.Empty) == 0)
                    context.DailyStatistics.InsertMany(Defaults.DailyStatistics);
            }

            app.UseExceptionHandler(builder =>
            {
                builder.Run(async reqContext =>
                {
                    reqContext.Response.StatusCode = 500;
                    reqContext.Response.ContentType = "application/json";
                    var errorHandler = reqContext.Features.Get<IExceptionHandlerFeature>();
                    if (errorHandler != null)
                    {
                        Console.WriteLine(errorHandler.Error);
                        await telegramNotifyService.SendMessage($"*Error occurred*\n{errorHandler.Error}");
                    }
                });
            });

            telegramNotifyService.Start().GetAwaiter().GetResult();
            lifetime.ApplicationStopped.Register(async () => await telegramNotifyService.Stop());

            app.UseCors(env.IsProduction() ? EnvironmentName.Production : EnvironmentName.Development)
                .UseStaticFiles().UseSwagger(o => o.RouteTemplate = Path.Combine("/docs/{documentName}/data.json")).UseSwaggerUI(options =>
                {
                    options.DefaultModelsExpandDepth(-1);
                    options.DocumentTitle = "Docs | Airsoft Amo";
                    options.RoutePrefix = "docs";
                    options.SupportedSubmitMethods();
                    options.SwaggerEndpoint("/docs/v1/data.json", "API v1");
                    options.InjectStylesheet("/theme-newspaper.css");
                }).UseMvc();
        }
	}
}

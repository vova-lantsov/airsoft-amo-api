using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AirsoftAmoApi.Models;

namespace AirsoftAmoApi.Extensions
{
    public static class ModelFormatExtensions
    {
        public static string FormatOrder(this Order order, List<Product> products, out string orderFormattedId)
        {
            decimal totalPrice = 0M;
            orderFormattedId = order.Id.ToString("D6");
            int i = 0;
            return $"*New order*\n\nId: _{orderFormattedId}_\nPhone number: _{order.PhoneNumber}_\nName: _{order.Name}_\nDelivery location: _{order.DeliveryLocation}_\n\n*Products:*\n" +
				string.Join('\n', order.Products.Select(p =>
				{
					var product = products.Find(p1 => p1.ProductId == p.ProductId);
					if (decimal.TryParse(product.ProductPrice, out var productPrice))
					{
                        productPrice *= p.Amount;
						totalPrice += productPrice;
					}
					return $"*{i++}.* {product.ProductName} x {p.Amount} = {FormatCurrency(productPrice)}";
				})) + $"\n\n*Total price: {FormatCurrency(totalPrice)}*";
        } 

        public static string FormatComment(this Comment comment, Product product)
        {
            return $"*New comment*\n\nName: _{comment.FirstName}_\nText: _{comment.CommentText}_\n\n*For product*\n"
				+ string.Join('\n', $"Name: _{product.ProductName}_", $"Category: _{product.Category}_");
        }

        private static string FormatCurrency(decimal value)
		{
			return value.ToString("C", CultureInfo.CreateSpecificCulture("en-US"));
		}
    }
}
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AirsoftAmoApi.Models;
using AirsoftAmoApi.Models.Admin;
using MlkPwgen;

namespace AirsoftAmoApi.Extensions
{
    public static class Defaults
    {
        public static Product[] Products => new[]
        {
            new Product
            {
                ProductId = 1L,
                ProductName = "M16A3 Katana System [WE]",
                Category = "rifles",
                ProductPrice = "606",
                ProductDescription = "M16A3 Rifle is the variant of the M16A2 which comes in Fully Automatic instead of the 3-round burst mode, equipped with removable carrying handle like M4A1 Carbine and full length handguard like M16A2. It has been used by US Navy & SEALS Team. WE KATANA M4A1 AEG is the new tactical rifle in WE AEG Series which comes in a new designed KATANA Split gear box system. The KATANA AEG system comes with a changeable cylinder unit and the new designed gear box in the lower frame. It will comes with 3 types of cylinder unit (300 / 400 / 450 FPS) and the 400 FPS cylinder is the standard eqippment with the KATANA AEG. Although the gear box is special made, it can compatible with most of the parts of  ver 2 gear box (gear set, selector plate and motor). When user need to change the cylinder unit, please ensure the selector is in “Safe” before changing. Also, the hop up system and inner barrel is compatible with TM M4 AEG standard, and it also comes with the same function already on their M4 AEG: When user turns to “Safe”, it will release the spring in the gear box back to normal state.",
                AmountLeft = "135",
                ImgUrl = "https://airsoft.hitman.com.ua/image/cache/catalog/products_2018/23150_16_large-max-800.jpg",
                Rating = 3,
                Comments = new List<Comment>
                {
                    new Comment
                    {
                        CommentId = 1L,
                        FirstName = "Alex",
                        CommentText = "As most of WE proucts quality is not as good as I want for this price. But it works great.",
                        CommentDate = "01/10/2018"
                    },
                    new Comment
                    {
                        CommentId = 2L,
                        FirstName = "Bob",
                        CommentText = "Nice one. Really want to order it.",
                        CommentDate = "02/10/2018"
                    }
                }
            },
            new Product
            {
                ProductId = 2L,
                ProductName = "AKC-74M E&L Gen2",
                Category = "rifles",
                ProductPrice = "407",
                ProductDescription = "E&L Airsoft is a new quality on the airsoft market. The manufacturer has set himself the task of creating replicas with the highest possible quality of materials and perfect fitting, resembling the real steels as close as possible and went in this direction as far as he could - by starting production at the same factory, which produced a firearm! Therefore E&L's products are made with use of steel, hand finished wood components and high-quality composite and definitely can compete with the best brands on the market - also in price. The quality and performance allow it to be both a beautiful wall decoration as well as very effective skirmish gun. Also, some parts of the exterior are the same as in the original weapon and come off the same production line! Externally, the maximum possible number of elements was made ​as ​a 1:1 copy of the original, fully according to military specifications. This means you can exchange elements between the replica and the real steel! This applies to items such as the front bed, gas pipe, iron sights and cleaning rod. Hardly any other manufacturer offers that. Steel components have been oxidized, which makes them look even more realistic. Also, the attrition process which takes place over time makes oxidized surfaces look much better than it happens by painted or varnished guns. Also in terms of weight  ​​all was made to ensure realism. The replica is heavy, and a full steel outer barrel ensures proper balance. A notable feature is certainly the possibility of a partial field-stripping of the replica, like it takes place with the a real steel - with no tools required. Everything is based on pins and levers. The manufacturer paid attention to even of the smallest of details to make their product as close a resemblence to the real steel as possible.",
                AmountLeft = "120",
                ImgUrl = "https://airsoft.hitman.com.ua/image/cache/catalog/E_L/1152202731_11-max-800.jpg",
                Rating = 5,
                Comments = new List<Comment>()
            },
            new Product
            {
                ProductId = 3L,
                ProductName = "Steyr AUG Sniper [Jing Gong]",
                Category = "sniper",
                ProductPrice = "319",
                ProductDescription = "Armee-Universal-Gewehr (Universal Army Rifle) or AUG is a futuristic assault rifle developed during the 1960s in Austria to replace the aging 7.62mm FAL automatic rifles in service at the time. Often considered as the first successful bullpup rifle, the AUG was indeed well ahead of its time as far as concept and design; it was highly advanced when it first went into service in the late 1970s. Its bullpup design which moves the core of the rifles action behind the trigger allows for the use of a full length rifle barrel in a weapon with the ergonomics of a sub-machine gun. Some other significant features include fully ambidextrous controls and reversible charging handle and shell ejection port allowing the rifle to be configured for both left and right handed shooters. The unique two stage trigger allows the user to fire in semi-auto and full-auto without needing to manipulate a selector switch; for semi-automatic fire, the operator will just pull the trigger half way and if fully-automatic fire was necessary, the operator needs to only pull the trigger all the way back. Chambered in the 5.56x45mm NATO cartridge for its ease of use and handling, the futuristic family of rifles in the AUG line can be quickly adapted to accommodate a wide variety of roles in the ever changing battlefield with just a quick swap of the barrel. The AUG is definitely a well-rounded combat weapon with a proven track record that can stand its own even with todays modern combat rifles.",
                AmountLeft = "120",
                ImgUrl = "https://airsoft.hitman.com.ua/image/cache/catalog/products_2018/0446A_1_-max-800.jpg",
                Rating = 0,
                Comments = new List<Comment>()
            },
            new Product
            {
                ProductId = 4L,
                ProductName = "TM Colt 1911 Night Warrior",
                Category = "pistols",
                ProductPrice = "139",
                ProductDescription = "One for 1911 lovers (or those who appreciate a nice gun) - the Tokyo Marui Night Warrior is essentially an M.E.U. with different externals. As you can probably tell, it differs from your bog-standard 1911 government pistol, with a trouser-stirring appearance thanks to the stunning grip, skeletoned trigger, hammer, bottom rail and unusual sights. The detailing is as good as any other Marui gun and it will surely have thoroughly solid performance as per-usual for a Tokyo Marui gun.",
                AmountLeft = "100",
                ImgUrl = "http://img-cdn.redwolfairsoft.com/upload/product/img/TM-GBB-NWAR-1L.jpg",
                Rating = 5,
                Comments = new List<Comment>()
            },
            new Product
            {
                ProductId = 5L,
                ProductName = "10 functional holster for Colt 1911",
                Category = "gear",
                ProductPrice = "110",
                ProductDescription = "Made of high quality plastic. Can be hold either on leg or belt. Includes: holster, tactical holster platform, high ride duty belt loop, CQC Paddle Platform.",
                AmountLeft = "100",
                ImgUrl = "https://hitman.com.ua/image/cache/catalog/products_2018/M51617043_TAN_1911_1_-max-800.jpg",
                Rating = 5,
                Comments = new List<Comment>()
            }
        };

        public static Admin[] Admins
        {
            get
            {
                var r = new System.Random(DateTime.Now.Millisecond);
                var cultureInfo = new CultureInfo("en-US");
                var admins = new[]
                {
                    new Admin
                    {
                        Avatar = "https://lolzteam.net/data/avatars/l/621/621469.jpg",
                        FirstName = "Vova",
                        LastName = "Lantsov",
                        Id = 1L,
                        Login = "vova.lantsov",
                        Type = "Administrator",
                        WorksSince = "08/12/2018"
                    },
                    new Admin
                    {
                        Avatar = "https://avatarfiles.alphacoders.com/865/86518.png",
                        FirstName = "Viacheslav",
                        LastName = "Zhuravskyi",
                        Id = 2L,
                        Login = "willson",
                        Type = "Administrator",
                        WorksSince = "08/12/2018"
                    },
                    new Admin
                    {
                        Avatar = "https://instagram.fiev4-1.fna.fbcdn.net/vp/d626e837272bbcd5813ff54453fea181/5CA3D49E/t51.2885-15/e35/23507305_378476072582801_5656701748237041664_n.jpg",
                        FirstName = "Rostyslav",
                        LastName = "Zosimov",
                        Id = 3L,
                        Login = "zosimov",
                        Type = "Moderator",
                        WorksSince = "08/12/2018"
                    }
                };
                foreach (var admin in admins)
                {
                    admin.AdminMonthsActivity = MonthlyStatistics;
                    admin.CanceledOrders = admin.AdminMonthsActivity.Sum(activity => activity.Canceled);
                    admin.CompletedOrders = admin.AdminMonthsActivity.Sum(activity => activity.Completed);
                }
                return admins;
            }
        }

        public static Credential[] AdminCredentials
        {
            get
            {
                var credentials = new Credential[3];
                string[] passwords =
                {
                    "athletic",
                    "penspining",
                    "rostyslav"
                };
                for (int index = 0; index < 3; ++index)
                {
                    var credential = new Credential
                    {
                        AdminId = index + 1,
                        Salt = PasswordGenerator.Generate(8)
                    };
                    credential.PasswordHash = PasswordHashExtensions.HashPassword(passwords[index], credential.Salt);
                    credentials[index] = credential;
                }
                return credentials;
            }
        }

        public static DailyStatistic[] DailyStatistics
        {
            get
            {
                var dailyStatistics = new DailyStatistic[30];
                var date = DateTime.UtcNow;
                for (int i = 29; i >= 0; --i)
                {
                    var dailyStatistic = new DailyStatistic
                    {
                        Date = date.ToString("dd/MM/yyyy"),
                        TotalOrders = _random.Next(101),
                        TotalPrice = _random.Next(101) * 500,
                        TotalSpent = _random.Next(101) * 100
                    };
                    dailyStatistics[i] = dailyStatistic;
                    if (i != 0)
                        date = date.AddDays(-1d);
                }
                return dailyStatistics;
            }
        }

        public static List<MonthlyStatistic> MonthlyStatistics
        {
            get
            {
                var months = _culture.DateTimeFormat.MonthNames;
                var statistics = new List<MonthlyStatistic>();
                for (int monthIndex = 0; monthIndex < 12; ++monthIndex)
                {
                    statistics.Add(new MonthlyStatistic
                    {
                        Canceled = _random.Next(20, 101),
                        Completed = _random.Next(20, 101),
                        Name = months[monthIndex]
                    });
                }
                return statistics;
            }
        }

        private static readonly System.Random _random = new System.Random();
        private static readonly CultureInfo _culture = new CultureInfo("en-US");
    }
}
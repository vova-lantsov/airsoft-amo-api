﻿using FluentValidation;
using System;
using System.IO;

namespace AirsoftAmoApi.Extensions
{
    public static class ValidationExtensions
    {
        public static IRuleBuilderOptions<T, string> Url<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder.Must(item =>
            {
                try
                {
                    var uri = new Uri(item, UriKind.Absolute);
                    var path = string.Format("{0}{1}{2}{3}", uri.Scheme, Uri.SchemeDelimiter, uri.Authority, uri.AbsolutePath);
                    var extension = Path.GetExtension(path);
                    return extension.In(".jpg", ".png", ".gif");
                }
                catch
                {
                    return false;
                }
            }).WithMessage("Item must be a valid url");
        }

        private static bool In(this string item, params string[] items)
        {
            foreach (var it in items)
                if (it == item)
                    return true;
            return false;
        }
    }
}
